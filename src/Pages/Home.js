import React from "react";

function Home() {
  return (
    <div className=" md:p-10 p-0 w-full h-5/6">
      <div className=" block md:flex justify-between h-full">
        <div className="block md:hidden mt-8 w-full sm:h-full h-72  bg-[url('./Assets/image-hero-mobile.png')] bg-cover sm:bg-cover  bg-no-repeat"></div>
        <div className=" md:w-1/2 w-full ">
          <div className="md:mt-10 sm:mt-10 mt-5 text-xl flex justify-center md:block font-bold md:text-6xl">
            <div>Make</div>
            <div className="ml-2">remote work</div>
          </div>
          <div className="pt-4 md:pt-7 text-sm md:text-xl sm:text-xl text-center md:text-left text-gray-400 font-semibold">
            <p>
              Get your team in sync,no matter your location. Streamline
              processes, create team rituals,and watch productivity soar.
            </p>
          </div>
          <div className="flex justify-center md:justify-start">
            <div className="mt-3 md:mt-10 sm:mt-10 p-2 px-4 w-32 rounded-lg text-center border-2 font-bold bg-black text-white cursor-pointer hover:bg-white hover:text-black hover:border-black">
              Learn More
            </div>
          </div>
          <div className="pt-10 ml-5 w-11/12 flex justify-between">
            <div className="w-32 h-10 gap-2  bg-[url('./Assets/client-databiz.svg')] bg-contain bg-no-repeat"></div>
            <div className="w-32 ml-5 bg-[url('./Assets/client-audiophile.svg')] bg-contain bg-no-repeat"></div>
            <div className="w-32 ml-5 bg-[url('./Assets/client-meet.svg')] bg-contain bg-no-repeat"></div>
            <div className="w-32 ml-5 bg-[url('./Assets/client-maker.svg')] bg-contain bg-no-repeat"></div>
          </div>
        </div>
        <div className="hidden md:block w-2/6 h-full  bg-[url('./Assets/image-hero-desktop.png')] bg-contain bg-no-repeat"></div>
      </div>
    </div>
  );
}

export default Home;
