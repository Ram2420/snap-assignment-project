import React from "react";
import { MenuItems } from "../Components/MenuItems";
import { useState } from "react";

function Navbar() {
  const [isSidebar, setIsSidebar] = useState(false);
  const [dropdownItems, setDropdownItems] = useState(MenuItems);

  const menuIcon =
    '<svg width="28" height="18" xmlns="http://www.w3.org/2000/svg"><g fill="#151515" fill-rule="evenodd"><path d="M0 0h32v2H0zM0 8h32v2H0zM0 16h32v2H0z"/></g></svg>';
  const closeMenuIcon =
    '<svg width="26" height="26" xmlns="http://www.w3.org/2000/svg"><g fill="#151515" fill-rule="evenodd"><path d="m2.393.98 22.628 22.628-1.414 1.414L.979 2.395z"/><path d="M.98 23.607 23.609.979l1.414 1.414L2.395 25.021z"/></g></svg>';

  const toggleSidebar = () => {
    setIsSidebar(!isSidebar);
  };
  const openDropDown = (itemIdex, item) => {
    const updatedDropdowns = dropdownItems.map((dropdown, index) => ({
      ...dropdown,
      isOpen: index === itemIdex ? !dropdown.isOpen : false,
    }));

    setDropdownItems(updatedDropdowns);
  };

  return (
    <div className="p-4">
      <div className="grid grid-cols-2 max-w-full ">
        <div className="flex items-center">
          <div className="min-w-28 h-8 bg-[url('./Assets/logo.svg')] bg-cover bg-no-repeat"></div>
          <div className="ml-10 w-full ">
            <div className="hidden md:flex justify-evenly">
              {dropdownItems.map((item, index) => (
                <div key={index} className="text-gray-400 font-semibold ">
                  {/* Other content or styling for each div */}
                  <div className="ml-10 flex items-center hover:text-gray-800 cursor-pointer">
                    {item.subMenu.length !== 0 && (
                      <div onClick={openDropDown.bind(null, index, item.menu)}>
                        <div>{item.menu}</div>
                      </div>
                    )}
                    {item.subMenu.length === 0 && <div>{item.menu}</div>}
                    {item.subMenu.length !== 0 && !item.isOpen && (
                      <div className="ml-2">
                        <svg
                          width="10"
                          height="6"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            stroke="#686868"
                            strokeWidth="1.5"
                            fill="none"
                            d="m1 1 4 4 4-4"
                          />
                        </svg>
                      </div>
                    )}
                    {item.subMenu.length !== 0 && item.isOpen && (
                      <div className="ml-2">
                        <svg
                          width="10"
                          height="6"
                          xmlns="http://www.w3.org/2000/svg"
                        >
                          <path
                            stroke="#686868"
                            strokeWidth="1.5"
                            fill="none"
                            d="m1 5 4-4 4 4"
                          />
                        </svg>
                      </div>
                    )}
                  </div>
                  {item.isOpen && (
                    <div className="p-5 mt-5 rounded-lg absolute shadow-lg bg-white ">
                      {item.subMenu.map((subItem, index) => (
                        <div
                          key={index}
                          className="mt-2 flex cursor-pointer hover:text-gray-800"
                        >
                          <div
                            dangerouslySetInnerHTML={{ __html: subItem.icon }}
                          ></div>
                          <div className="ml-2">{subItem.title}</div>
                        </div>
                      ))}
                    </div>
                  )}
                </div>
              ))}
            </div>
          </div>
        </div>
        <div>
          <div className="flex justify-end items-center text-gray-400 font-semibold">
            <div className="hidden md:block mr-24 hover:text-gray-800 cursor-pointer  ">
              Login
            </div>
            <div className="hidden md:block p-2 px-4 hover:text-gray-800 cursor-pointer border-2 rounded-xl border-gray-400 hover:border-gray-800 ">
              Register
            </div>
            <div
              onClick={toggleSidebar}
              className=" mt-2 cursor-pointer flex items-center md:hidden"
              dangerouslySetInnerHTML={{ __html: menuIcon }}
            ></div>
          </div>
        </div>
      </div>

      {/* Sidebar */}

      {isSidebar && (
        <div className="w-full absolute top-0 right-0 h-screen bg-black opacity-80  z-10"></div>
      )}
      {isSidebar && (
        <div className="p-5 w-1/2 absolute top-0 right-0 h-screen bg-white  z-30 transform transition-transform duration-300">
          <div className="flex justify-end">
            <div
              onClick={toggleSidebar}
              className="cursor-pointer"
              dangerouslySetInnerHTML={{ __html: closeMenuIcon }}
            ></div>
          </div>
          <div className="">
            {dropdownItems.map((item, index) => (
              <div key={index} className="text-gray-600 font-semibold ">
                {/* Other content or styling for each div */}
                <div className="mt-5 flex items-center hover:text-gray-800 cursor-pointer">
                  {item.subMenu.length !== 0 && (
                    <div onClick={openDropDown.bind(null, index, item.menu)}>
                      <div>{item.menu}</div>
                    </div>
                  )}
                  {item.subMenu.length === 0 && <div>{item.menu}</div>}
                  {item.subMenu.length !== 0 && !item.isOpen && (
                    <div className="ml-2">
                      <svg
                        width="10"
                        height="6"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          stroke="#686868"
                          strokeWidth="1.5"
                          fill="none"
                          d="m1 1 4 4 4-4"
                        />
                      </svg>
                    </div>
                  )}
                  {item.subMenu.length !== 0 && item.isOpen && (
                    <div className="ml-2">
                      <svg
                        width="10"
                        height="6"
                        xmlns="http://www.w3.org/2000/svg"
                      >
                        <path
                          stroke="#686868"
                          strokeWidth="1.5"
                          fill="none"
                          d="m1 5 4-4 4 4"
                        />
                      </svg>
                    </div>
                  )}
                </div>
                {item.isOpen && (
                  <div className="p-2 ">
                    {item.subMenu.map((subItem, index) => (
                      <div
                        key={index}
                        className="mt-2 flex cursor-pointer hover:text-gray-800"
                      >
                        <div
                          dangerouslySetInnerHTML={{ __html: subItem.icon }}
                        ></div>
                        <div className="ml-2">{subItem.title}</div>
                      </div>
                    ))}
                  </div>
                )}
              </div>
            ))}
          </div>
          <div>
            <div className="mt-8 text-center text-gray-600 font-semibold">
              <div className=" hover:text-gray-800 cursor-pointer  ">Login</div>
              <div className="p-1 px-6 mt-4 hover:text-gray-800 cursor-pointer border-2 rounded-xl border-gray-600 hover:border-gray-800 ">
                Register
              </div>
            </div>
          </div>
        </div>
      )}
    </div>
  );
}

export default Navbar;
