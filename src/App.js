import Navbar from "./Components/Navbar";
import Home from "./Pages/Home";

function App() {
  return (
    <div className=" md:p-8 w-full  md:h-screen sm:h-screen h-screen text-sm bg-slate-100">
      <Navbar />
      <Home />
    </div>
  );
}

export default App;
